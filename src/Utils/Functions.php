<?php
namespace GF\Themebuilder\Utils;

use GF\Themebuilder\Controllers\Bootstrap;
use GF\Themebuilder\Controllers\Customizer;

/**
 * Bootstrap theme builder
 *
 * @return Page_Builder_Controller
 */
function bootstrapThemebuilder()
{
    getBootstrapThemebuilder()->install();
}

/**
 * Get bootstrap
 *
 * @return Bootstrap
 */
function getBootstrapThemebuilder()
{
    return Bootstrap::getInstance();
}

/**
 * Init customizer
 *
 * @return void
 */
function initCustomizer(){
    $c = Customizer::getInstance();
    return $c->install();
}