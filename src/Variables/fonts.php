<?php


$font_choices = array(
    '@import "fonts/heuristica";"Heuristica", serif' => 'Heuristica (Standard för rubriker)',
    '@import "fonts/source_sans_pro";"SourceSansPro", sans-serif' => "SourceSansPro (Standard för brödtext)",
    '@import "fonts/ibm_plex_sans";"IBM Plex Sans", sans-serif' => "IBM Plex Sans",
    '@import "fonts/toledo_serial";"Toledo-serial", sans-serif' => "Toledo Serial", // Added 2020-02-04
    '@import "fonts/toledo_xlight";"Toledo-xlight", sans-serif' => "Toledo Xlight", // Added 2020-02-27
    '@import url(https://fonts.googleapis.com/css?family=Libre+Baskerville);"Libre Baskerville", serif' => "Libre Baskerville",
    '@import url(https://fonts.googleapis.com/css?family=Frank+Ruhl+Libre);"Frank Ruhl Libre", serif'  => "Frank Ruhl Libre",
    '@import url(https://fonts.googleapis.com/css?family=IBM+Plex+Serif);"IBM Plex Serif", serif' => "IBM Plex Serif",
    '@import url(https://fonts.googleapis.com/css?family=Manuale);"Manuale", serif' => "Manuale",
    '@import url(https://fonts.googleapis.com/css?family=Open+Sans);"Open Sans", sans-serif' => "Open Sans",
    '@import url(https://fonts.googleapis.com/css?family=Montserrat);"Montserrat", sans-serif;' => "Montserrat",
    '@import url(https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed);"Barlow Semi Condensed", sans-serif;' => "Barlow Semi Condensed",


    '@import url(https://fonts.googleapis.com/css?family=Poppins);"Poppins", sans-serif' => 'Poppins', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Space+Mono);"Space Mono", monospace' => 'Space Mono', // Added 2020-02-04


    '@import url(https://fonts.googleapis.com/css?family=Alegreya);"Alegreya", serif' => 'Alegreya', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Vollkorn);"Vollkorn", serif' => 'Vollkorn', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=BioRhyme);"BioRhyme", serif' => 'BioRhyme', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Spectral);"Spectral", serif' => 'Spectral', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Cormorant);"Cormorant", serif' => 'Cormorant', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Lora);"Lora", serif' => 'Lora', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Cardo);"Cardo", serif' => 'Cardo', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Playfair+Display);"Playfair Display", serif' => 'Playfair Display', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Frank+Ruhl+Libr);"Frank Ruhl Libr", serif' => 'Frank RuhlLibr', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Crimson+Text);"Crimson Text", serif' => 'Crimson Text', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=B612);"B612", sans-serif' => 'B612', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Mulish);"Mulish", sans-serif' => 'Mulish', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Titillium+Web);"Titillium Web", sans-serif' => 'Titillium Web', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Varela);"Varela", sans-serif' => 'Varela', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Cairo);"Cairo", sans-serif' => 'Cairo', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Karla);"Karla", sans-serif' => 'Karla', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Archivo);"Archivo", sans-serif' => 'Archivo', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Fjalla+One);"Fjalla One", sans-serif' => 'Fjalla One', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Rubik);"Rubik", sans-serif' => 'Rubik', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Work+Sans);"Work Sans", sans-serif' => 'Work Sans', // Added 2020-02-04
    '@import url(https://fonts.googleapis.com/css?family=Concert+One);"Concert One", cursive' => 'Concert One', // Added 2020-02-04


    'Times new roman' => 'Times new roman',
    'Arial,Helvetica Neue,Helvetica,sans-serif' => 'Arial',
    'Helvetica Neue,Helvetica,Arial,sans-serif' => 'Helvetica',
    'Avant Garde,Avantgarde,Century Gothic,CenturyGothic,AppleGothic,sans-serif' => 'Avant Garde',
    'Century Gothic,CenturyGothic,AppleGothic,sans-serif' => 'Century Gothic',
    'Lucida Grande,Lucida Sans Unicode,Lucida Sans,Geneva,Verdana,sans-serif' => 'Lucida Grande',
    'Verdana,Geneva,sans-serif' => 'Verdana',
    'Garamond,Baskerville,Baskerville Old Face,Hoefler Text,Times New Roman,serif' => 'Garamond',
);


return [
    "font-family-secondary" => array(
        "title" => __("Headings", "grafikfabriken"),
        "default" => array_keys($font_choices)[0],
        "type" => "select",
        "choices" => $font_choices
    ),
    "font-family-base" => array(
        "title" => __("Body text", "grafikfabriken"),
        "default" => array_keys($font_choices)[1],
        "type" => "select",
        "choices" => $font_choices
    )
];
