<?php
return array(
    "primary" => array(
        "title" => __('Primary', 'grafikfabriken'),
        "default" => "#2e3e51"
    ),
    "secondary" => array(
        "title" => __('Secondary', 'grafikfabriken'),
        "default" => "#537488"
    ),
    "tertiary" => array(
        "title" => __('Tertiary', 'grafikfabriken'),
        "default" => "#efedea"
    ),
    "quaternary" => array(
        "title" => __('Quaternary', 'grafikfabriken'),
        "default" => "#537488"
    ),
    "primary_two" => array(
        "title" => __('Rubriker', 'grafikfabriken'),
        "default" => "#537488"
    ),
    "secondary_two" => array(
        "title" => __('HR färg', 'grafikfabriken'),
        "default" => "#537488"
    )
);
// $primary: #6b6360; // färg 1 i block manualen
// $secondary: #cbbfb0; // färg 2 i block manualen
// $tertiary: #f2f0ee; // färg 3 i block manualen
// $quaternary: #97523b; // färg 4 i block manualen