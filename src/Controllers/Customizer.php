<?php

namespace GF\Themebuilder\Controllers;

use GF\Utils\Singleton;
use ScssPhp\ScssPhp\Compiler;
use function GF\Utils\cacheController;

/**
 * Main Customizer class
 */
class Customizer extends Singleton
{

    /**
     * name
     *
     * @var string
     */
    public $panel_name;

    /**
     * Pretty name
     *
     * @var string
     */
    public $panel_pretty_name;

    /**
     * Colors
     *
     * @var array
     */
    public $colors;

    /**
     * Font sizes
     *
     * @var array
     */
    public $font_sizes;

    /**
     * Grid settings
     *
     * @var array
     */
    public $grid;

    /**
     * Wp customizer
     *
     * @var \WP_Customize_Manager
     */
    public $customizer;

    /**
     * Scss
     *
     * @var Compiler
     */
    public $scss;

    /**
     * Option name in DB
     *
     * @var string
     */
    public $option_name = "";

    /**
     * Construct method
     *
     * @return void
     */
    public function _construct()
    {

        $this->panel_name = 'themebuilder';

        $this->panel_pretty_name = __('Themebuilder', 'grafikfabriken');

        $this->colors = include_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'Variables/colors.php');

        $this->fonts = include_once(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'Variables/fonts.php');

        $this->option_name = "gf_ovveride_theme_url";

        add_action('wp_enqueue_scripts', array($this, 'maybee_ovveride_css'), 11);
    }

    /**
     * Install customizer
     *
     * @return void
     */
    public function install()
    {
        add_action('customize_register', array($this, 'register'));
        add_action('customize_preview_init', array($this, 'live_preview'));
        add_action('customize_save_after', array($this, 'save'));
        add_action('customize_controls_print_scripts', array($this, 'customize_controls_print_scripts'));
        add_action('wp_ajax_customizer_reset', array($this, 'ajax_customizer_reset'));
    }

    /**
     * Enqueue script to customizer!
     */
    public function customize_controls_print_scripts()
    {
        wp_enqueue_script('customizer-reset', THEME_BUILDER_SCRIPTS_URL . 'script.js', array('jquery'));
        wp_localize_script('customizer-reset', '_ZoomCustomizerReset', array(
            'reset'   => __('Reset', 'grafikfabriken'),
            'confirm' => __("Attention! This will remove all customizations for the theme colors and fonts!", 'grafikfabriken'),
            'nonce'   => array(
                'reset' => wp_create_nonce('customizer-reset'),
            )
        ));
    }

    /**
     * Check ajax call
     *
     * @return void
     */
    public function ajax_customizer_reset()
    {
        if (!$this->customizer->is_preview()) {
            wp_send_json_error('not_preview');
        }

        if (!check_ajax_referer('customizer-reset', 'nonce', false)) {
            wp_send_json_error('invalid_nonce');
        }

        $this->reset_customizer();

        wp_send_json_success();
    }

    /**
     * Reset customizer
     *
     * @return void
     */
    public function reset_customizer()
    {

        //Remove theme mod
        remove_theme_mod('themebuilder');

        //Remove css
        delete_option($this->option_name);
    }

    /**
     * Maybee ovveride default css
     *
     * @return void
     */
    public function maybee_ovveride_css()
    {

        if (!is_customize_preview()) {
            $url = get_option($this->option_name);

            if (filter_var($url, FILTER_VALIDATE_URL)) {
                wp_dequeue_style('gf_app_css');
                wp_register_style('gf_app_css-css', $url);
                wp_enqueue_style('gf_app_css-css');
            }
        }
    }

    /**
     * Register settings to customizer
     *
     * @param \WP_Customize_Manager $wp_customize
     * @return void
     */
    public function register($wp_customize)
    {

        $this->customizer = $wp_customize;

        $wp_customize->add_panel($this->panel_name, array(
            'priority' => 10,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => $this->panel_pretty_name,
            'description' => __('Genereate custom scss', 'grafikfabriken'),
        ));

        $this->add_colors();

        //Font section
        $this->add_generic_section(
            'font',
            __('Fonts', 'grafikfabriken'),
            __('Change font apparence', 'grafikfabriken'),
            $this->fonts
        );
    }

    /**
     * Get setting id
     *
     * @param string $type
     * @param string $key
     * @return string
     */
    public function get_setting_id($type, $key)
    {
        return $this->panel_name . '[' . $type . '][' . $key . ']';
    }

    public function get_section_id($type)
    {
        return $this->panel_name . '_' . $type;
    }

    public function add_section($type, $name, $desc, $priority)
    {

        $id = $this->get_section_id($type);



        $this->customizer->add_section(
            $id,
            array(
                'title' => $name, //Visible title of section
                'priority' => $priority, //Determines what order this appears in
                'capability' => 'edit_theme_options', //Capability needed to tweak
                'description' => $desc, //Descriptive tooltip
                'panel' => $this->panel_name
            )

        );

        return $id;
    }

    public function add_setting($id, $default)
    {
        $this->customizer->add_setting(
            $id,
            array(
                'default' => $default, //Default setting/value to save
                'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
            )
        );
    }

    public function add_control($id, $title, $section, $type = "text", $priority = 10, $choices = array())
    {
        switch ($type) {
            case 'color':
                $this->customizer->add_control(new \WP_Customize_Color_Control(
                    $this->customizer,
                    $id,
                    array(
                        'label' => $title,
                        'settings' => $id,
                        'priority' => $priority,
                        'section' => $section,
                    )
                ));
                break;
            case 'button':
                $this->customizer->add_control(
                    $id, //Set a unique ID for the control
                    array(
                        'type' => $type,
                        'label' => $title, //Admin-visible name of the control
                        'settings' => $id, //Which setting to load and manipulate (serialized is okay)
                        'priority' => $priority, //Determines the order this control appears in for the specified section
                        'section' => $section, //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                        'choices' => $choices
                    )
                );
                break;
            case 'select':
                $this->customizer->add_control(
                    $id, //Set a unique ID for the control
                    array(
                        'type' => $type,
                        'label' => $title, //Admin-visible name of the control
                        'settings' => $id, //Which setting to load and manipulate (serialized is okay)
                        'priority' => $priority, //Determines the order this control appears in for the specified section
                        'section' => $section, //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                        'choices' => $choices
                    )
                );
                break;
            case 'number':
            case 'text':
                $this->customizer->add_control(
                    $id, //Set a unique ID for the control
                    array(
                        'type' => $type,
                        'label' => $title, //Admin-visible name of the control
                        'settings' => $id, //Which setting to load and manipulate (serialized is okay)
                        'priority' => $priority, //Determines the order this control appears in for the specified section
                        'section' => $section, //ID of the section this control should render in (can be one of yours, or a WordPress default section)
                    )
                );
            default:
                # code...
                break;
        }
    }

    public function add_generic_section($key, $title, $description, $data)
    {
        $section_id = $this->add_section($key, $title, $description, 0);
        foreach ($data as $k => $data) {
            $id = $this->get_setting_id($key, $k);
            $this->add_setting($id, $data["default"]);
            $type = isset($data["type"]) ? $data["type"] : "text";
            $choices = isset($data["choices"]) ? $data["choices"] : array();
            $this->add_control($id, $data["title"], $section_id, $type, 1, $choices);
        }
    }

    public function add_colors()
    {

        $section_id = $this->add_section('colors', __('Colors', 'grafikfabriken'), __('Change your colors', 'grafikfabriken'), 0);
        // dpr($section_id,2);
        foreach ($this->colors as $key => $color) {
            $id = $this->get_setting_id('colors', $key);
            $this->add_setting($id, $color["default"]);

            if (isset($color["type"])) {
                $this->add_control($id, $color["title"], $section_id, $color['type'], 1);
            } else {
                $this->add_control($id, $color["title"], $section_id, 'color', 1);
            }
        }
    }

    /**
     * Live preview
     *
     * @return void
     */
    public function live_preview()
    {
        $css = $this->compile();
        if ($css != "") {
            add_action('wp_head', function () use ($css) {
                // dpr($css);
                echo "<!--GF Live preview css-->";
                echo sprintf("<style>%s</style>", $css);
                wp_deregister_style('gf_app_css-css');
            }, 999);
        }
    }

    /**
     * Save options
     *
     * @return void
     */
    public function save()
    {
        $css = $this->compile();

        if ($css !== "") {
            $save = $this->save_file($css);
            cacheController()->delete_all_cache();
        }
    }

    public function compile()
    {
        if (\is_customize_preview()) {

            //Get themebuilder mod
            $theme_mod = get_theme_mod("themebuilder");

            //New str
            $str = "";

            if (array_has_items($theme_mod)) {
                $str = "/** Generated from Wp theme customizer */\n";
                foreach ($theme_mod as $key => $data) {
                    $str .= "\n/** {$key} */\n";
                    if (array_has_items($data)) {
                        foreach ($data as $prop => $value) {
                            if (!empty($value)) {

                                if (strpos($value, "@import") !== false) {
                                    $values = explode(";", $value);
                                    foreach ($values as $v) {
                                        if (strpos($v, "@import") !== false) {
                                            $str .= str_replace('@import "', '@import "' . GF_THEME_ASSETS_ENTRY_DIR . '/styles/1_settings/', $v) . ";\n";
                                        } else {
                                            $str .= "\${$prop}: $v;\n";
                                        }
                                    }
                                } else {
                                    $str .= "\${$prop}: $value;\n";
                                }
                            }
                        }
                    }
                }
            }

            $this->scss = new Compiler();
            $this->scss->addImportPath(GF_THEME_BS_DIR);
            $this->scss->addImportPath(GF_THEME_BS_SELECT_DIR);
            $this->scss->addImportPath(GF_THEME_ASSETS_ENTRY_DIR);


            $result = "";

            try {

                $main_scss = file_get_contents(GF_THEME_ASSETS_ENTRY_STYLE_FILE);
                $main_scss = $str . $main_scss;
                $result = $this->scss->compile($main_scss);
            } catch (\Exception $e) {


                //Do something
                var_dump($e);
            }

            return $result;
        }
    }

    /**
     * Save file
     *
     * @param string $css
     * @return bool
     */
    public function save_file($css)
    {

        if (!function_exists('WP_Filesystem')) {
            require_once(ABSPATH . 'wp-admin/includes/file.php');
        }

        try {


            //code...
            /** @var \WP_Filesystem $wp_filesystem */
            global $wp_filesystem;

            if (!$wp_filesystem) {
                WP_Filesystem(["verbose" => true], false, true);
            }


            $blog_id = get_current_blog_id();

            $upload_dir = wp_upload_dir();

            $upload_dir_path = $upload_dir['basedir'] . '/app/css/';
            $upload_url_path = $upload_dir['baseurl'] . '/app/css/';

            $css_file_name = 'app' . '_' . $blog_id . '_' . time() . '.css';
            $css_file_path = $upload_dir_path . $css_file_name;
            $css_file_url = $upload_url_path . $css_file_name;

            $manage_upload = apply_filters('manage_theme_upload_css', false, $css_file_name, $css_file_path, $css);

            // dpr($wp_filesystem,2);

            //Abillity to hook in
            if ($manage_upload === false && $wp_filesystem) {


                if ($upload_dir_path && $wp_filesystem && !$wp_filesystem->is_dir($upload_dir_path)) {
                    $r = wp_mkdir_p($upload_dir_path);
                }

                if (file_exists($css_file_path)) {
                    // $wp_filesystem->delete($css_file_path);
                    wp_delete_file($css_file_path);
                }

                $result = $wp_filesystem->put_contents(
                    $css_file_path,
                    $css,
                    FS_CHMOD_FILE
                );


                if ($result) {
                    $manage_upload = $css_file_url;
                }
            }



            if (is_string($manage_upload) && strlen($manage_upload) > 0) {

                update_option($this->option_name, $manage_upload, true);
            }

            return true;
        } catch (\Exception $e) {
            return true;
        }
    }
}
