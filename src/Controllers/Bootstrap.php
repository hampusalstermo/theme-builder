<?php

namespace GF\Themebuilder\Controllers;

use GF\Utils\Singleton;
use function GF\Themebuilder\Utils\initCustomizer;
use wpCloud\StatelessMedia\ThemeSupport;

class Bootstrap extends Singleton{

    public $css_upload;

    /**
     * Install theme builder
     *
     * @return void
     */
    public function install(){ 

        if(!defined('THEME_BUILDER_SCRIPTS_URL')){
            define('THEME_BUILDER_SCRIPTS_URL', str_replace('/themes/grafikfabriken/app', '/themes/grafikfabriken', GF_THEME_URL) . DIRECTORY_SEPARATOR . 'vendor/grafikfabriken/theme-builder/src/Assets/Scripts/');
        }

        initCustomizer();
    }

}